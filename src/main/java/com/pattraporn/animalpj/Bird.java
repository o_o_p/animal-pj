/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.animalpj;

/**
 *
 * @author Pattrapon N
 */
public class Bird extends Poultry{
    private String NameBird;
    
    public  Bird(String NameBird) {
        super(" Bird", 2);
        this.NameBird=NameBird;
    }

    @Override
    public void fly() {
        System.out.println("Bird: "+NameBird+" i can fly with 2 wings");
    }

    @Override
    public void eat() {
        System.out.println("Bird: "+NameBird+" eat worm");
    }

    @Override
    public void move() {
        System.out.println("Bird: "+NameBird+" move around");
    }

    @Override
    public void speak() {
        System.out.println("Bird: "+NameBird+" jip jip");
    }

    @Override
    public void sleep() {
        System.out.println("Bird: "+NameBird+" Zzzzzzz");
    }
}
