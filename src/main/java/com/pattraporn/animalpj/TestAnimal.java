/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.animalpj;

/**
 *
 * @author Pattrapon N
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 =new Human("Dang");
        h1.eat();
        h1.move();
        h1.run();
        h1.sleep();
        h1.speak();
        System.out.println("h1 is animal?"+(h1 instanceof Animal));
        System.out.println("h1 is LandAnimal?"+(h1 instanceof LandAnimal));
        Animal a1 = h1;
        System.out.println("a1 is animal?"+(a1 instanceof Animal));
        System.out.println("a1 is Reptile?"+(a1 instanceof Reptile));
        Cat c1 =new Cat("Meow");
        c1.eat();
        c1.move();
        c1.run();
        c1.sleep();
        c1.speak();
        System.out.println("c1 is animal?"+(c1 instanceof Animal));
        System.out.println("c1 is LandAnimal?"+(c1 instanceof LandAnimal));
        Dog d1 =new Dog("Tonton");
        d1.eat();
        d1.move();
        d1.run();
        d1.sleep();
        d1.speak();
        System.out.println("d1 is animal?"+(d1 instanceof Animal));
        System.out.println("d1 is LandAnimal?"+(d1 instanceof LandAnimal));
        Crocodile cr1 =new Crocodile("Alex");
        cr1.eat();
        cr1.move();
        cr1.crawl();
        cr1.sleep();
        cr1.speak();
        System.out.println("cr1 is animal?"+(cr1 instanceof Animal));
        System.out.println("cr1 is Reptile?"+(cr1 instanceof Reptile));
        
        Snake s1 =new Snake("sasuke");
        s1.eat();
        s1.move();
        s1.crawl();
        s1.sleep();
        s1.speak();
        System.out.println("s1 is animal?"+(s1 instanceof Animal));
        System.out.println("s1 is Reptile?"+(s1 instanceof Reptile));
        
        
        Fish f1 =new Fish("Ella");
        f1.eat();
        f1.move();
        f1.swim();
        f1.sleep();
        f1.speak();
        System.out.println("f1 is animal?"+(f1 instanceof Animal));
        System.out.println("f1 is AquaticAnimal?"+(f1 instanceof AquaticAnimal));
        Crab crab =new Crab("Staphan");
        crab.eat();
        crab.move();
        crab.swim();
        crab.sleep();
        crab.speak();
        System.out.println("crab is animal?"+(crab instanceof Animal));
        System.out.println("crab is AquaticAnimal?"+(crab instanceof AquaticAnimal));
        Bat bat =new Bat("Bruce");
        bat.eat();
        bat.move();
        bat.fly();
        bat.sleep();
        bat.speak();
        System.out.println("bat is animal?"+(bat instanceof Animal));
        System.out.println("bat is Poultry?"+(bat instanceof Poultry));
        Bird bird =new Bird("iruka");
        bird.eat();
        bird.move();
        bird.fly();
        bird.sleep();
        bird.speak();
        System.out.println("bird is animal?"+(bird instanceof Animal));
        System.out.println("bird is Poultry?"+(bird instanceof Poultry));
       
        
}
}