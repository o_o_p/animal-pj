/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.animalpj;

/**
 *
 * @author Pattrapon N
 */
public class Dog extends LandAnimal{
     private String Namedog;
    
    public Dog(String Namedog) {
        super("Dog", 4);
        this.Namedog=Namedog;
    }

    @Override
    public void run() {
        System.out.println("Dog: "+Namedog+" run with me");
    }

    @Override
    public void eat() {
        System.out.println("Dog: "+Namedog+" eat beef");
    }

    @Override
    public void move() {
        System.out.println("Dog: "+Namedog+" move around");
    }

    @Override
    public void speak() {
        System.out.println("Dog: "+Namedog+" wolf wolf!");
    }

    @Override
    public void sleep() {
        System.out.println("Dog: "+Namedog+" Zzzzzzz");
    }
    
}
