/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.animalpj;

/**
 *
 * @author Pattrapon N
 */
public class Snake extends Reptile{
     private String NameSnake;
    
    public Snake(String NameSnake) {
        super("Snake", 0);
        this.NameSnake=NameSnake;
    }

    @Override
    public void crawl() {
        System.out.println("Snake: "+NameSnake+" crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snake: "+NameSnake+" eat rat");
    }

    @Override
    public void move() {
        System.out.println("Snake: "+NameSnake+" move around");
    }

    @Override
    public void speak() {
        System.out.println("Snake: "+NameSnake+" shhhh");
    }

    @Override
    public void sleep() {
        System.out.println("Snake: "+NameSnake+" Zzzzzzz");
    } 
}
