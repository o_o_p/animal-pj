/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.animalpj;

/**
 *
 * @author Pattrapon N
 */
public class Bat extends Poultry{
     private String NameBat;
    
    public  Bat(String NameBat) {
        super(" Bat", 2);
        this.NameBat=NameBat;
    }

    @Override
    public void fly() {
        System.out.println("Bat: "+NameBat+" i can fly with 2 wings");
    }

    @Override
    public void eat() {
        System.out.println("Bat: "+NameBat+" eat banana");
    }

    @Override
    public void move() {
        System.out.println("Bat: "+NameBat+" move around");
    }

    @Override
    public void speak() {
        System.out.println("Bat: "+NameBat+" .........");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: "+NameBat+" Zzzzzzz");
    }
}
