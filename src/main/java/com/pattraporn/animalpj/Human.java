/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.animalpj;

/**
 *
 * @author Pattrapon N
 */
public class Human extends LandAnimal{
    private String nickname;
    
    public Human(String nickname) {
        super("Human", 2);
        this.nickname=nickname;
    }

    @Override
    public void run() {
        System.out.println("Human: "+nickname+" run with 2 legs");
    }

    @Override
    public void eat() {
        System.out.println("Human: "+nickname+" eat food!!");
    }

    @Override
    public void move() {
        System.out.println("Human: "+nickname+" move around");
    }

    @Override
    public void speak() {
        System.out.println("Human: "+nickname+" say Hello!!");
    }

    @Override
    public void sleep() {
        System.out.println("Human: "+nickname+"Zzzzzzz");
    }
    
}
