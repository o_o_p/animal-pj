/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.animalpj;

/**
 *
 * @author Pattrapon N
 */
public class Cat extends LandAnimal{
    private String Namecat;
    
    public Cat(String Namecat) {
        super("Cat", 4);
        this.Namecat=Namecat;
    }

    @Override
    public void run() {
        System.out.println("Cat: "+Namecat+" run to me");
    }

    @Override
    public void eat() {
        System.out.println("Cat: "+Namecat+" eat Me-o");
    }

    @Override
    public void move() {
        System.out.println("Cat: "+Namecat+" move around");
    }

    @Override
    public void speak() {
        System.out.println("Cat: "+Namecat+" Meow meow!");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: "+Namecat+" Zzzzzzz");
    }
    
}

